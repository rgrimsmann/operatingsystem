#ifndef __STRING_H
#define __STRING_H
/*Converts a integer in a string aka char**/
unsigned char* itoa(int i); 
/*Converts a hex int to string aka char* */
unsigned char* htoa(int h);
#endif