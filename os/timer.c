
#include <system.h>

long timer_ticks = 0;
long max_cnt = 36;
unsigned short *textmemptr = (unsigned short *)0xB8000;
void timer_handler(struct regs *r)
{
    
    timer_ticks++;
     
    /*if(timer_ticks >= max_cnt) */
   // Timer callback
}


void timer_wait(int ticks)
{
    volatile unsigned long eticks = timer_ticks + ticks;
    while(timer_ticks <= eticks);
    // Warten, bis timer_ticks gross genug ist
}


void timer_install()
{
    
    irq_install_handler(0, timer_handler);
}
