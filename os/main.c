
#include <system.h>

#define SEC(i) (i * 1000)

extern void init_PIT();

void *memcpy(void *dest, const void *src, size_t count)
{
    char* tmpD = (char*)dest;
    char* tmpS = (char*)src;
    for(; count >= 0; count--) {
        *tmpD++ = *tmpS++;
    }
    return 0;
}

void *memset(void *dest, char val, size_t count)
{
    char *temp = (char *)dest;
    for( ; count != 0; count--) *temp++ = val;
    return dest;
}

unsigned short *memsetw(unsigned short *dest, unsigned short val, size_t count)
{
    unsigned short *temp = (unsigned short *)dest;
    for( ; count != 0; count--) *temp++ = val;
    return dest;
}

size_t strlen(const char *str)
{
    size_t retval;
    for(retval = 0; *str != '\0'; str++) retval++;
    return retval;
}

unsigned char inportb (unsigned short _port)
{
    unsigned char rv;
    __asm__ __volatile__ ("inb %1, %0" : "=a" (rv) : "dN" (_port));
    return rv;
}

void outportb (unsigned short _port, unsigned char _data)
{
    __asm__ __volatile__ ("outb %1, %0" : : "dN" (_port), "a" (_data));
}

void irq8_default(struct regs *r) {
    puts("Interrupt 8\n");
    outportb(0x70, 0x0C);
        inportb(0x71);
}

void init_rtc(void) {
    char prev;
    __asm__ __volatile__("cli");
    outportb(0x70, 0x8A);
    outportb(0x71, 0x20);
    //End setting registers
    outportb(0x70, 0x8B);
    prev = inportb(0x71);
    outportb(0x70, 0x8B);
    outportb(0x71, prev | 0x40);
    //end Turning on interrupts
    outportb(0x70, 0x8A);		// set index to register A, disable NMI
    prev=inportb(0x71);	// get initial value of register A
    outportb(0x70, 0x8A);		// reset index to A
    outportb(0x71, (prev & 0xF0) | 3);
    //end set rate
    __asm__ __volatile__("sti");
}

void main()
{
    init_video();
    puts((unsigned char*)"Init video [DONE]\n");
    gdt_install();
    puts((unsigned char*)"Init gdt [DONE]\n");
    idt_install();
    puts((unsigned char*)"Init idt [DONE]\n");
    isrs_install();
    puts((unsigned char*)"Init isrs [DONE]\n");
    irq_install();
    puts((unsigned char*)"Init irq [DONE]\n"); 

    init_PIT();
    __asm__ __volatile__("sti");
    timer_install();
    keyboard_install();
    init_rtc();
    irq_install_handler(8, irq8_default);
    //memcpy((unsigned short*)(0xB8000 + (20*160)),(unsigned short *)0xB8000,800);
    // timer_wait(SEC(5));
  //  memcpy((unsigned short*)(0xB8000 + (20*160)),(unsigned short *)0xB8000,800);
    //puts((unsigned char*)"Hello World!\n");
    
    for (;;);
}
