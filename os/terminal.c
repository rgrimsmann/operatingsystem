#include <system.h>
#include <terminal.h>

unsigned char* history = 0;
unsigned char line = 0;
unsigned char* LastEntry(void) {
    return NULL;
}
/*Get The entry in order as they are safed*/
unsigned char* GetEntry(void) {
    return history + line;
}
void SetEntry(const char* str) {
    char len = strlen(str) +1;
    memcpy(history + line, str, len);
    memcpy(history + line + len, 0, 1);
    if(line < LINES)
        line++;
    else 
        line = 0;
}

void Terminal_Init(void) {
    history = (unsigned char*)0xC2000;
    memset(history, 0, (80 * 25));
}