CC = gcc
FLAGS = -m32 -march=i386 -Wall -O0 -ffreestanding -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o
OBJ = string.o start.o main.o screen.o gdt.o idt.o isrs.o irq.o timer.o keyboard.o
all: kernel.bin 
	mount -o loop grub+kernel.bin.img /mnt/fdtmp/
	cp kernel.bin /mnt/fdtmp/
	umount /mnt/fdtmp/
clean: *.o 
	rm -rf *.o kernel.bin 

kernel.bin: $(OBJ)
	ld -m elf_i386 -T link.ld --oformat binary -o kernel.bin $(OBJ)
	
start.o : start.asm 
	nasm -f elf32 -o start.o start.asm

%.o: %.c
	$(CC) $(FLAGS) $@ $<
